(function($){
    $.fn.slideImageGallery = function(options) {

        var slideHorizontalMargin = 0;
        var buttonHeight = 0;
        var currentSlide = 0;
        var totalSlides = 0;
        var slides = [];
        var slideWidths = [];
        var slideLoaded = [];
        var loading = true;

        var $gallery;
        var $leftButton;
        var $rightButton;
        var $galleryContainer;
        var $loading;
        var $caption;
        var $galleryImage;
        var $backgroundPanel;

        var defaults = {
            preloadSlides : 3,
            loadingMessageDelay : 2000,
            loadingMessageSpeed : 1200,
            loadingMessageMinOpacity : 0.4,
            loadingMessageMaxOpacity : 1,
            captionSpeed : 1200,
            captionOpacity : 0.5,
            leftKeyCode : 37,
            rightKeyCode : 39,
            currentSlideOpacity : 1.0,
            backgroundSlideOpacity : 0.5,
            hideInactiveSlides : false,
            captionAlwaysVisible : false
        };

        var setStyles = function(){
            $backgroundPanel = $('<div></div>');
            $backgroundPanel.attr('id', 'backgroundPanel');

            $leftButton = $('<button></button>');
            $leftButton.attr('id', 'leftButton');
            $leftButton.html('&lt;');
            $('body').prepend($leftButton);

            $rightButton = $('<button></button>');
            $rightButton.attr('id', 'rightButton');
            $rightButton.html('&gt;');
            $('body').prepend($rightButton);

            $galleryContainer = $('<div></div>');
            $galleryContainer.attr('id', 'galleryContainer');

            $gallery = $('<div></div>');
            $gallery.attr('id', 'gallery');
            $galleryContainer.append($gallery);

            $caption = $('<div></div>');
            $caption.attr('id', 'caption');
            $galleryContainer.append($caption);

            $loading = $('<div></div>');
            $loading.attr('id', 'loading');
            $loading.html('Please wait...');
            $galleryContainer.append($loading);

            $galleryImage= $('img');
            $galleryImage.appendTo($gallery);

            $backgroundPanel.append($galleryContainer);
            $('body').append($backgroundPanel);
        };

        var newOptions = $.extend(defaults, options);

        var moveRight = function () {

            if (currentSlide == totalSlides - 1) return;
            if (slideWidths[currentSlide + 1] == undefined) return;

            slides[currentSlide].unbind('mouseenter').unbind('mouseleave');
            if(!newOptions.captionAlwaysVisible)
                $caption.stop().clearQueue().hide();

            var offset = slideWidths[currentSlide] / 2 + slideHorizontalMargin * 2 + slideWidths[currentSlide + 1] / 2;
            $gallery.animate({ left:'-=' + offset });

            if(newOptions.hideInactiveSlides){
                slides[currentSlide].fadeTo('slow', .0);
                slides[currentSlide+1].fadeTo('slow', newOptions.currentSlideOpacity, function(){
                    $caption.html(slides[currentSlide].attr('alt'));
                });
            } else {
                slides[currentSlide].fadeTo('slow', newOptions.backgroundSlideOpacity);
                slides[currentSlide+1].fadeTo('slow', newOptions.currentSlideOpacity, function(){
                    $caption.html(slides[currentSlide].attr('alt'));
                });
            }
            currentSlide++;

            setButtonStates();

            if(!newOptions.captionAlwaysVisible)
                 addSlideHover();
        };

        var init = function() {

            setStyles();

            slideHorizontalMargin = parseInt($galleryImage.css('margin-left'));
            $gallery.fadeTo(0, 0).css('top','-999em');
            buttonHeight = $leftButton.css('height');
            $leftButton.css('height',0);
            $rightButton.css('height',0);

            $loading.delay(newOptions.loadingMessageDelay);
            fadeInLoadingMessage();

            $galleryImage.load(handleSlideLoad);

            $galleryImage.each( function() {
                $(this).hide().data('slideNum', totalSlides);
                slides[totalSlides++] = $(this);
                if (this.complete) $(this).trigger('load');
                $(this).attr('src', $(this).attr('src'));
            });

            $(window).resize(centreCurrentSlide);

            setButtonStates();

            $caption.html(slides[currentSlide].attr('alt'));

            $(document).on('keydown', function(event) {
                if (event.which == newOptions.leftKeyCode) moveLeft();
                if (event.which == newOptions.rightKeyCode) moveRight();
            });

            $leftButton.on('click', moveLeft);
            $rightButton.on('click', moveRight);

        };

        var handleSlideLoad = function() {
            slideWidths[$(this).data('slideNum')] = $(this).width();
            $gallery.width($gallery.width() + $(this).width() + slideHorizontalMargin*2);
            slideLoaded[$(this).data('slideNum')] = true;

            if (loading) {

                var preloaded = 0;

                for (var i=0; i < newOptions.preloadSlides; i++) {
                    if (slideLoaded[i]) preloaded++;
                }

                if ( preloaded == newOptions.preloadSlides || preloaded == totalSlides ) {
                    $loading.clearQueue().stop().fadeTo('slow', 0 );
                    $gallery.css('top',0);
                    $gallery.fadeTo('slow', 1 );
                    $leftButton.css('height',buttonHeight);
                    $rightButton.css('height',buttonHeight);
                    $rightButton.show();
                    if(!newOptions.captionAlwaysVisible)
                        addSlideHover();
                    loading = false;

                    if(newOptions.captionAlwaysVisible)
                        $caption.stop().fadeTo(newOptions.captionSpeed, newOptions.captionOpacity);
                }
            }

            if ($(this).data('slideNum') == 0) {
                centreCurrentSlide();
                $(this).fadeTo('slow', newOptions.currentSlideOpacity);
            } else {
                if(!newOptions.hideInactiveSlides)
                    $(this).fadeTo('slow', newOptions.backgroundSlideOpacity);
            }

        };

        var moveLeft = function() {

            if (currentSlide == 0) return;
            if (slideWidths[currentSlide-1] == undefined) return;

            slides[currentSlide].unbind('mouseenter').unbind('mouseleave');
            if(!newOptions.captionAlwaysVisible)
                $caption.stop().clearQueue().hide();

            var offset = slideWidths[currentSlide]/2 + slideHorizontalMargin*2 + slideWidths[currentSlide-1]/2;
            $gallery.animate({ left: '+=' + offset });

            if(newOptions.hideInactiveSlides){
                slides[currentSlide].fadeTo('slow', .0);
                slides[currentSlide-1].fadeTo('slow', newOptions.currentSlideOpacity, function(){
                    $caption.html(slides[currentSlide].attr('alt'));
                });
            } else {
                slides[currentSlide].fadeTo('slow', newOptions.backgroundSlideOpacity);
                slides[currentSlide-1].fadeTo('slow', newOptions.currentSlideOpacity, function(){
                    $caption.html(slides[currentSlide].attr('alt'));
                });
            }


            currentSlide--;

            setButtonStates();

            if(!newOptions.captionAlwaysVisible)
                addSlideHover();
        };

        var centreCurrentSlide = function() {

            var offsetFromGalleryStart = 0;

            for (var i=0; i<currentSlide; i++) {
                offsetFromGalleryStart += slideWidths[i] + slideHorizontalMargin*2;
            }

            var windowCentre = $(window).width() / 2;
            var slideLeftPos = windowCentre - ( slideWidths[currentSlide] / 2 );
            var offset = slideLeftPos - offsetFromGalleryStart - slideHorizontalMargin;

            $gallery.css( 'left', offset );
        };

        var setButtonStates = function() {

            if (currentSlide == 0) {
                $leftButton.hide();
            } else {
                $leftButton.show();
            }

            if (currentSlide == totalSlides - 1) {
                $rightButton.hide();
            } else {
                $rightButton.show();
            }

        };

        var addSlideHover = function() {
            slides[currentSlide].hover(
                function() { $caption.stop().fadeTo(newOptions.captionSpeed, newOptions.captionOpacity)},
                function() { $caption.stop().fadeTo(newOptions.captionSpeed, 0 )}
            );
        };

        var fadeInLoadingMessage = function() {
            $loading.animate({opacity: newOptions.loadingMessageMaxOpacity}, newOptions.loadingMessageSpeed, 'swing', fadeOutLoadingMessage);
        };

        var fadeOutLoadingMessage = function () {
            $loading.animate({opacity:newOptions.loadingMessageMinOpacity}, newOptions.loadingMessageSpeed, 'swing', fadeInLoadingMessage);
        };

        init();

    };
})(jQuery);